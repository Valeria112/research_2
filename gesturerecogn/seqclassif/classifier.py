import logging

import numpy
import cv2
from gesturerecogn.abstract import AbstractClassifier


def normalize(seq):
    max_point = (max(seq[:, 0]), max(seq[:, 1]))
    min_point = (min(seq[:, 0]), min(seq[:, 1]))

    coeff = max_point[0] - min_point[0] \
        if max_point[0] - min_point[0] > max_point[1] - min_point[1] \
        else max_point[1] - min_point[1]

    seq -= [min_point] * len(seq)
    seq /= [[coeff, coeff]] * len(seq)

    return seq


class SequenceClassifier(AbstractClassifier):
    def classify(self, obj):
        return self.predict(obj)

    def __init__(self, models):
        self.models = models
        self.nums = len(models)

    def predict(self, trajectory, norm_traject=True):
        return numpy.argmax(self.predict_proba(trajectory, norm_traject))

    def predict_proba(self, trajectory, norm_traject=True):
        if norm_traject:
            trajectory = cv2.normalize(trajectory, norm_type=cv2.NORM_MINMAX)

        return [model._decode_viterbi(trajectory)[0] for model in self.models]

    def predict_diff(self, trajectory, norm_traject=False):
        probs = self.predict_proba(trajectory, norm_traject)

        abs_min = abs(numpy.min(probs))

        diff = (numpy.max(probs) + abs_min) / (numpy.sum(probs) + self.nums * abs_min) * self.nums
        logging.info('diff: %s', diff)

        return numpy.argmax(probs), diff > 1.3
