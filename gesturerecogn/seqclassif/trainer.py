import logging
import numpy as np
from hmmlearn import hmm

from gesturerecogn import SettingProvider, SequenceClassifier
from gesturerecogn.abstract import AbstractTrainer


class SequenceTrainer(AbstractTrainer):
    def __init__(self, n_segments=8, max_jump=1, ):
        self.__n_segments = n_segments
        self.__max_jump = max_jump

        self.__models = []

    def _init_start_prob(self):
        return np.ones(self.__n_segments) / self.__n_segments

    def _init_trans_mat(self):
        trans_mat = np.zeros((self.__n_segments, self.__n_segments))

        for i in xrange(self.__n_segments):
            for j in xrange(self.__n_segments):
                if i <= j <= i + self.__max_jump:
                    trans_mat[i, j] = 1.0 / self.__n_segments
            norm = trans_mat[i].sum()
            for j in xrange(self.__n_segments):
                trans_mat[i, j] /= norm

        return trans_mat

    def _init_means_covariance(self, data):
        dim = len(data[0][0])

        means = np.zeros((self.__n_segments, dim))
        covariance = np.zeros((self.__n_segments, dim))

        for temp_seg in xrange(self.__n_segments):
            points = data[:, temp_seg, :]

            means[temp_seg] = np.mean(points, axis=0)
            covariance[temp_seg] = [np.cov(points[:, d]) for d in xrange(dim)]

        return means, covariance

    def train(self, train_data):
        gesture_num = len(train_data)
        self.__models = []

        for num in xrange(gesture_num):
            data = [self._break_seq(seq) for seq in train_data[num]]

            means, covariance = self._init_means_covariance(np.array(data))

            model = hmm.GaussianHMM(self.__n_segments, "diag", self._init_start_prob(), self._init_trans_mat())
            model.init_params = ''

            model.means_ = means

            try:
                model.covars_ = covariance
            except ValueError:
                model.init_params = 'c'

            try:
                model.fit(np.array(train_data[num]))
            except ValueError:
                logging.exception('Model did not fitting')

            self.__models.append(model)

        return self.__models

    def save(self):
        SettingProvider.write_models_params(self.__models)

    def _break_seq(self, data):
        sequence_len, dimension = data.shape
        dist = lambda a, b: np.linalg.norm(a - b)
        traject_len = lambda: np.sum([dist(data[ind - 1], data[ind]) for ind in range(1, len(data))])

        get_point = lambda start_point, end_point, interpolate_position: np.array([
                                                                                      (1 - interpolate_position) *
                                                                                      start_point[
                                                                                          i] + interpolate_position *
                                                                                      end_point[i]
                                                                                      for i in range(dimension)])

        interpolated_seq = []
        desired_segment_len = traject_len() / (self.__n_segments - 1)
        current_segment_len = 0
        last_test_point = data[0]
        interpolated_seq.append(last_test_point)

        index = 1
        while index < sequence_len:
            current_point = data[index]
            inc_to_current_length = dist(last_test_point, current_point)
            test_segment_length = current_segment_len + inc_to_current_length

            if test_segment_length < desired_segment_len:
                current_segment_len = test_segment_length
                last_test_point = current_point
                index += 1
                continue

            interpolation_position = (desired_segment_len - current_segment_len) / inc_to_current_length

            interpolated_point = get_point(last_test_point, current_point, interpolation_position)
            interpolated_seq.append(interpolated_point)

            if len(interpolated_seq) == self.__n_segments - 1:
                break

            last_test_point = interpolated_point
            current_segment_len = 0

        interpolated_seq.append(data[-1])
        return np.array(interpolated_seq)

    @property
    def classifier(self):
        return SequenceClassifier(self.__models)
