import numpy as np
import cv2


class MorphTransform(object):
    def __init__(self, kernel):
        self.__kernel = np.ones(kernel, np.uint8)

    @property
    def kernel(self):
        return self.__kernel

    @kernel.setter
    def kernel(self, value):
        self.__kernel = np.ones(value, np.uint8)

    def erosion(self, image, num_iter=1):
        return cv2.erode(image, self.__kernel, num_iter)

    def dilation(self, image, num_iter=1):
        return cv2.dilate(image, self.__kernel, num_iter)

    def erosion_and_dilation(self, image, num_iter=1):
        return cv2.dilate(
            cv2.erode(image, self.__kernel, num_iter),
            self.__kernel,
            num_iter)

    def opening(self, image):
        return cv2.morphologyEx(image, cv2.MORPH_OPEN, self.__kernel)

    def closing(self, image):
        return cv2.morphologyEx(image, cv2.MORPH_CLOSE, self.__kernel)

    def opening_and_closing(self, image):
        return self.opening(self.closing(image))

    def apply_all(self, image, num_iter=1):
        return self.erosion_and_dilation(self.opening_and_closing(image), num_iter)