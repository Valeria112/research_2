import copy

import cv2

import hands as ch
from functions import nothing


class Tracker(object):
    # template matching accuracy threshold
    __THRESHOLD = 0.75

    # maximum number of frames in a row
    # did not pass the threshold
    __MAX_DELETE_COUNT = 3

    # minimum hand velocity
    # pixels / millisecond
    __MIN_HAND_VELOCITY = 50

    # tracking methods
    TEMPLATE_MATCHING = 0
    MEAN_SHIFT = 1

    # term criteria for meanShift method
    __TERM_CRITERIA = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 40, 1)

    def __init__(self, hands):
        # that stores the current hand.
        self.currentHands = hands
        self.method = cv2.TM_CCOEFF_NORMED

    def update(self, frame, method):
        switch = [self.template_matching, self.mean_shift, nothing]
        index = lambda i: i if i in xrange(len(switch)) else -1

        return switch[index(method)](frame)

    def template_matching(self, frame):
        """
        find hands from currentHands using matchTemplate from OpenCV on frame

        :param frame: rgb frame from capture
        :return: nothing to return, use currentHands elements
        """

        for hand in self.currentHands:
            template = copy.deepcopy(hand.gray_image)

            x0, y0, x1, y1 = hand.roi_skin
            template = template[y0:y1, x0:x1]
            w, h = template.shape[::-1]

            gray_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            x0, y0, x1, y1 = Tracker.region_of_tracking(
                gray_frame.shape[::-1], (hand.last_location.x, hand.last_location.y), (w, h))

            res = cv2.matchTemplate(gray_frame[y0: y1, x0: x1], template, self.method)
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

            center = ch.Point(max_loc[0] + x0 + w / 2, max_loc[1] + y0 + h / 2)
            hand.add_location(center)

            if max_val > Tracker.__THRESHOLD:
                hand.reset_delete_count()
            else:
                hand.add_delete_count()

        self.delete_hands()

    def mean_shift(self, frame):
        """
        find hands from currentHands using meanShift from OpenCV on frame
        :param frame: rgb frame from capture
        """
        for hand in self.currentHands:
            hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

            prob_image = cv2.calcBackProject([hsv_frame], [0], hand.hist, [0, 180], 1)

            x0, y0, x1, y1 = hand.roi_skin
            x, y, w, h = hand.track_window
            track_window = (x + x0, y + y0, x1 - x0, y1 - y0)

            ret, (x, y, w, h) = cv2.meanShift(prob_image, track_window, Tracker.__TERM_CRITERIA)

            count_non_zero = cv2.countNonZero(prob_image[y: y + h, x: x + w]) / float(w * h)

            hand.add_location(ch.Point(x + w / 2, y + h / 2))

            if count_non_zero > self.__THRESHOLD:
                hand.reset_delete_count()
            else:
                hand.add_delete_count()

        self.delete_hands()

    def delete_hands(self):
        for hand in self.currentHands:
            if hand.delete_count > Tracker.__MAX_DELETE_COUNT:
                self.currentHands.delete_elem(hand)

    @staticmethod
    def region_of_tracking((w_frame, h_frame), (center_x, center_y), (w, h)):
        from functions import low_point, upper_point

        return upper_point(center_x - w), upper_point(center_y - h), \
            low_point(center_x + w, w_frame), low_point(center_y + h, h_frame)
