import math
import cv2
import numpy
import time

from drawer import MAIN_WINDOW_SIZE
from functions import hypotenuse, upper_point


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __str__(self):
        return 'Point(' + str(self.x) + ', ' + str(self.y) + ')'

    def distance_to_other_point(self, point):
        return hypotenuse(self.x - point.x,
                          self.y - point.y)

    @property
    def as_tuple(self):
        return self.x, self.y

    @property
    def as_list(self):
        return [self.x, self.y]

    @property
    def reflected(self):
        return Point(self.x, MAIN_WINDOW_SIZE.HEIGHT - self.y)


def find_white_rect(bin_img, seg):
    w, h = bin_img.shape[::-1]
    step_w = h / seg
    step_h = w / seg
    max_val = 0
    max_roi = (0, 0, w, h)
    for x_size in xrange(2 * step_w, w, step_w):
        for y_size in xrange(2 * step_h, h, step_h):
            for x in xrange(0, w, step_w):
                for y in xrange(0, h, step_h):
                    tmp_val = math.pow(
                        cv2.countNonZero(bin_img[y:y + y_size, x:x + x_size]), 1.7
                    ) / float(x_size * y_size)
                    if tmp_val >= max_val:
                        max_val = tmp_val
                        max_roi = (x, y, x + x_size, y + y_size)
    return max_roi


class CurrentHands(object):
    class Hand(object):
        def __init__(self, img, mask, num, start_location, meter):
            self.rgb_image = img
            self.hsv_image = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
            self.gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            self.mask = mask

            self.w, self.h = self.gray_image.shape[::-1]

            self.roi_skin = find_white_rect(mask, seg=10)

            # for mean shift options
            x0, y0, x1, y1 = self.roi_skin
            self.hist = cv2.calcHist([self.hsv_image[y0:y1, x0:x1]], [0],
                                     self.mask[y0:y1, x0:x1], [180], [0, 180])
            cv2.normalize(self.hist, self.hist, 0, 255, cv2.NORM_MINMAX)

            self.locations = []
            self.features = []
            self.times = []
            self.velocities = []

            self.__delete_count = 0
            self.__len_trajectory = 0
            self.__num = num
            self.__last_segment_len = 0

            self.meter = meter
            self.locations.append(start_location)
            self.features.append([])

        def __str__(self):
            return self.gray_image

        def add_location(self, location):
            self.__last_segment_len = location.distance_to_other_point(self.last_location)
            self.__len_trajectory += self.__last_segment_len

            self.locations.append(location)
            self.times.append(time.time())
            self.velocities.append(self.velocity)

        def reset_locations(self):
            self.locations = [self.last_location]
            self.__len_trajectory = 0

        def add_delete_count(self):
            self.__delete_count += 1

        def reset_delete_count(self):
            self.__delete_count = 0

        @property
        def delete_count(self):
            return self.__delete_count

        @property
        def get_trajectory(self):
            return numpy.array(
                [self.locations[i].reflected.as_list
                 for i in range(len(self.locations))],
                dtype=numpy.float)

        @property
        def last_location(self):
            return self.locations[-1]

        @property
        def len_trajectory(self):
            return self.__len_trajectory

        @property
        def number(self):
            return self.__num

        @property
        def len_last_segment(self):
            return self.__last_segment_len

        @property
        def velocity(self):
            return self.len_last_segment / self.meter.last_frame_time

        @property
        def track_window(self):
            return (upper_point(self.last_location.x - self.w / 2),
                    upper_point(self.last_location.y - self.h / 2),
                    self.w, self.h)

        def print_locations(self):
            for l in self.locations:
                print l

    def __init__(self):
        self.__hands = []

    def __len__(self):
        return len(self.__hands)

    def __getitem__(self, item):
        return self.__hands[item]

    def __delitem__(self, item):
        del self.__hands[item]

    def delete_elem(self, elem):
        self.__hands.remove(elem)

    def add_hand(self, image, mask, location, meter=None):
        y, x = image.shape[:-1]
        new_hand = True

        for hand in self.__hands:
            y1, x1 = hand.gray_image.shape[::-1]
            if location.distance_to_other_point(hand.last_location) < hypotenuse(x, y) / 2 + hypotenuse(x1, y1) / 2:
                new_hand = False
                break

        if new_hand:
            self.__hands.append(
                self.Hand(img=image,
                          mask=mask,
                          num=len(self.__hands),
                          start_location=location,
                          meter=meter))