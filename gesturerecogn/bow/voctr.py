import cv2
import numpy as np


class VocabularyTrainer(object):

    def __init__(self, cluster_count, term_criteria=None, attempts=3, flag=cv2.KMEANS_PP_CENTERS):
        """
        :param cluster_count: number of count in the vocabulary (vocabulary size)
        :param term_criteria: term criteria for k-means algorithm
        :param attempts: attempts for k-means algorithm
        :param flag: flag for k-means algorithm
        """
        self.__cluster_count = cluster_count
        self.__term_criteria = term_criteria

        if not term_criteria:
            self.__term_criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_MAX_ITER, 10, 0.001)

        self.__attempts = attempts
        self.__flag = flag

        self.__descriptors = []

    def add_descriptor(self, descriptor):
        self.__descriptors.append(descriptor)

    def add_descriptors(self, descriptors):
        for descriptor in descriptors:
            self.add_descriptor(descriptor)

    def cluster(self):
        ret, labels, centers = cv2.kmeans(data=self.descriptors,
                                          K=self.__cluster_count,
                                          criteria=self.__term_criteria,
                                          attempts=self.__attempts,
                                          flags=self.__flag)
        return centers

    @property
    def descriptors(self):
        return np.float32(self.__descriptors)
