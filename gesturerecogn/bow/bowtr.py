import logging
import cv2
import numpy as np
from random import shuffle

from gesturerecogn import SettingProvider, DescriptorExtractor, BowClassifier
from gesturerecogn.abstract import AbstractTrainer
from gesturerecogn.bow.voctr import VocabularyTrainer


class BowTrainer(AbstractTrainer):
    def __init__(self, vocabulary_size=35):
        self.__vocabulary_size = vocabulary_size
        self.__poses_names = []
        self.__extractor = None
        self.__svm = cv2.SVM()

    @staticmethod
    def re_load(data):
        train_data, train_responses = [], []

        for i, img_type in enumerate(data):
            train_data += img_type
            train_responses += [[i]] * len(img_type)

        return train_data, train_responses

    def train(self, data):
        train_data, train_responses = BowTrainer.re_load(data=data)

        logging.info('Detect key points and compute their descriptors...')
        sift = cv2.SIFT(nfeatures=SettingProvider.N_FEATURES)
        vocabulary_trainer = VocabularyTrainer(self.__vocabulary_size)

        for img in train_data:
            kp, des = sift.detectAndCompute(img, mask=None)
            vocabulary_trainer.add_descriptors(des)

        logging.info('K-means vocabulary clustering...')
        vocabulary = vocabulary_trainer.cluster()

        logging.info('Create DescriptorExtractor and compute features vector for each image...')
        self.__extractor = DescriptorExtractor(vocabulary)
        features = []

        for img in train_data:
            kp, des = sift.detectAndCompute(img, mask=None)
            hist = self.__extractor.calculation_hist(des, normalize=True)
            features.append(hist)

        logging.info('Create SVM classifier...')
        svm_data = np.float32(features)
        responses = np.float32(train_responses)

        logging.info('Train SVM classifier...')

        svm_params = dict(kernel_type=cv2.SVM_RBF, svm_type=cv2.SVM_C_SVC)
        self.__svm.train_auto(svm_data, responses, None, None, svm_params)

        logging.info('BOW train end.')

    @property
    def descriptor_extractor(self):
        return self.__extractor

    @property
    def svm(self):
        return self.__svm

    @property
    def poses_names(self):
        return self.__poses_names

    @property
    def classifier(self):
        return BowClassifier(svm=self.svm, des_extractor=self.descriptor_extractor)
