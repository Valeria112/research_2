import cv2
import numpy


class DescriptorExtractor(object):

    def __init__(self, vocabulary):
        self.__voc_size = len(vocabulary)
        self.__vocabulary = vocabulary
        self.__bf_matcher = cv2.BFMatcher()

    @property
    def vocabulary(self):
        return self.__vocabulary

    def calculation_hist(self, descriptors, normalize=False):
        matcher = self.__bf_matcher.knnMatch(descriptors, self.vocabulary, k=1)
        extract = numpy.zeros(self.__voc_size, dtype=numpy.float32)

        for match in matcher:
            extract[match[0].trainIdx] += 1.0

        if normalize:
            for index, item in enumerate(cv2.normalize(extract, norm_type=cv2.NORM_INF)):
                extract[index] = item

        return extract
