import cv2

from gesturerecogn import SettingProvider
from gesturerecogn.abstract import AbstractClassifier


class BowClassifier(AbstractClassifier):

    def __init__(self, svm, des_extractor):
        self.__sift = cv2.SIFT(nfeatures=SettingProvider.N_FEATURES)
        self.__svm = svm
        self.__extractor = des_extractor

    def classify(self, img, mask=None):
        kp, des = self.__sift.detectAndCompute(img, mask=mask)
        hist = self.__extractor.calculation_hist(des, normalize=True)

        return self.__svm.predict(hist)
