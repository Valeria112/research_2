import cv2
import numpy as np


class ALL_COLORS:
    RED = (0, 0, 255)
    GREEN = (0, 255, 0)
    BLUE = (255, 0, 0)
    PURPLE = (255, 0, 255)
    WHITE = (255, 255, 255)
    BLACK = (0, 0, 0)
    GRAY = (128, 128, 128)


class MAIN_WINDOW_SIZE:
    HEIGHT = 640
    WIDTH = 480


class GESTURE_COLORS:
    __colors = (ALL_COLORS.RED, ALL_COLORS.GREEN, ALL_COLORS.BLUE, ALL_COLORS.PURPLE)

    @staticmethod
    def get_gesture_color(gesture_class):
        return GESTURE_COLORS.__colors[gesture_class]


class Drawer:

    @staticmethod
    def draw_point_rect(img, point, color, size=4, thickness=1):
        half_size = size / 2
        top_left_point = (point[0] - half_size, point[1] - half_size)
        bottom_right_point = (top_left_point[0] + size, top_left_point[1] + size)
        cv2.rectangle(img, top_left_point, bottom_right_point, color, thickness)

    @staticmethod
    def lighten_image(img, kernel_size=(9, 9)):
        return cv2.GaussianBlur(img, kernel_size, 0)

    @staticmethod
    def draw_trajectory(trajectory, img=None, color=ALL_COLORS.RED, thickness=1):
        if img is None:
            img = np.zeros((250, 250, 3), np.uint8)
            img[:, :] = ALL_COLORS.WHITE

        width, height = img.shape[:-1]
        count = len(trajectory)
        if count > 1:
            p1 = (int(trajectory[0, 0] * height), int((1 - trajectory[0, 1]) * width))
            Drawer.draw_point_rect(img, p1, color, size=4 * thickness, thickness=thickness)
            for i in xrange(0, count - 1):
                p2 = (int(trajectory[i + 1, 0] * height), int((1 - trajectory[i + 1, 1]) * width))
                cv2.line(img, p1, p2, color, thickness)
                p1 = p2
        return img

    @staticmethod
    def draw_db_trajectories(data, images=None, colors=GESTURE_COLORS):
        if not images:
            images = []
            for num in xrange(0, len(data)):
                images.append(np.zeros((MAIN_WINDOW_SIZE.WIDTH, MAIN_WINDOW_SIZE.HEIGHT, 3), np.uint8))
                images[num][:, :] = ALL_COLORS.WHITE

        elif len(images) == 1:
            images *= 4

        for num in xrange(0, len(data)):
            for trajectory in data[num]:
                Drawer.draw_trajectory(trajectory, images[num], colors.get_gesture_color(num))

        return images

    @staticmethod
    def draw_track(track, img, color, visible_points=None):
        track_len = len(track)

        if not visible_points or visible_points > track_len:
            visible_points = track_len

        for i in xrange(track_len - visible_points, track_len - 1):
            cv2.line(img, track[i].as_tuple, track[i + 1].as_tuple, color, 3)

    @staticmethod
    def draw_hist(data_columns, max_data, width_column=10, img_height=400):
        img_width = width_column * len(data_columns)
        height_segment_y = int(img_height / max_data)

        image = np.zeros((img_height, img_width, 3), np.uint8)

        for i, val in enumerate(data_columns):
            top_left = (i * width_column, 0)
            bottom_right = ((i + 1) * width_column, img_height - int(height_segment_y * val))

            cv2.rectangle(image, top_left, bottom_right, (255, 255, 255), -1)

        return image
