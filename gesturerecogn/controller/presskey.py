from pykeyboard import PyKeyboard


class HotKeyPresser(object):

    def __init__(self):
        self.__key_board = PyKeyboard()

    def win_switch(self):
        self.__key_board.press_key(self.__key_board.alt_key)
        self.__key_board.tap_key(self.__key_board.tab_key)

        self.__key_board.release_key(self.__key_board.alt_key)
        self.__key_board.release_key(self.__key_board.tab_key)

    def desktop_switch(self):
        self.__key_board.press_keys([
            self.__key_board.control_key,
            self.__key_board.alt_key,
            self.__key_board.right_key
        ])
