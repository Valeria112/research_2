from pymouse import PyMouse


class MouseSync(PyMouse):
    def __init__(self, frame_size=None):
        super(MouseSync, self).__init__()

        self._scale_x = 1
        self._scale_y = 1

        if frame_size:
            self.set_frame_size(frame_size)

    def set_frame_size(self, frame_size):
        screen_w, screen_h = self.screen_size()

        self._scale_x = screen_w / float(frame_size[0])
        self._scale_y = screen_h / float(frame_size[1])

    def translate_coordinates(self, point):
        new_x = round(point.x * self._scale_x)
        new_y = round(point.y * self._scale_y)

        return new_x, new_y