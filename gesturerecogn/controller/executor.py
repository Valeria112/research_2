import os
import logging


class Executor(object):
    def __init__(self, commands):
        self.__commands_list = commands

    def do_sys_action(self, action_num):
        if action_num in range(0, len(self.__commands_list)):
            os.system(self.__commands_list[action_num])
        else:
            raise IndexError('Index %s in out of range', action_num)

    def add_new_action(self, command):
        if self._check(command):
            self.__commands_list.appen(command)
        else:
            logging.exception('Command %s is not verified', command)

    @staticmethod
    def _check(command):
        return True
