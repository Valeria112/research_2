import time


class Meter:
    def __init__(self):
        self.__frame_count = 0
        self.__fps = 0.0
        self.__start_time = time.time()

        self.__last_time = self.__start_time
        self.__current_time = self.__start_time

        self.__total_time = self.__start_time

    def update(self):
        self.__frame_count += 1
        self.__last_time = self.__current_time
        self.__current_time = time.time()

        self.__total_time = self.__current_time - self.__start_time
        self.__fps = self.__frame_count / self.__total_time

    @property
    def mean_frame_time(self):
        return 1 / self.__fps

    @property
    def last_frame_time(self):
        return self.__current_time - self.__last_time

    @property
    def frame_count(self):
        return self.__frame_count

    @property
    def fps(self):
        return self.__fps
