import logging
import pickle
from random import shuffle
import xml.etree.ElementTree
import numpy as np
import glob as gb
import cv2
from os import listdir, path, makedirs
from collections import defaultdict


class SettingProvider:
    __SETTING_FILE_NAME = 'data/setting.xml'
    __TREE = xml.etree.ElementTree.parse(__SETTING_FILE_NAME)

    # Skin color mask
    HSV_VALUES_NAMES = ('h_min', 'h_max', 's_min', 's_max', 'v_min', 'v_max')

    # Sebastien Marcel - Hand Posture and Gesture Datasets:
    # http://www.idiap.ch/resource/gestures/data/shp_marcel_train.tar.gz
    ROOT_TRAIN_DIR = '/home/valeria/Downloads/Marcel-Train/'
    # http://www.idiap.ch/resource/gestures/data/shp_marcel_test.tar.gz
    ROOT_TEST_DIR = '/home/valeria/Downloads/Marcel-Test'
    TEST_SUB_DIR = '/home/valeria/Downloads/Marcel-Test/MiniTrieschGallery'

    # My hand data set
    MY_HAND_SET_DIR = '/home/valeria/my_hand_set/'

    # Bag of words film
    BOW_DATA_DIR = './data/badofwordsparams/'
    FILE_NAME_POSES = path.join(BOW_DATA_DIR, 'names.pickle')
    FILE_NAME_VOCABULARY = path.join(BOW_DATA_DIR, 'vocabulary.pickle')
    FILE_NAME_SVM = path.join(BOW_DATA_DIR, 'svm_classifier.dat')

    # Bag of words params
    DATA_SIZE = 1500
    VOCABULARY_SIZE = 100
    N_FEATURES = 35

    # Gestures Recognition Train Data DIR
    GR_ROOT_DIR = '/home/valeria/Downloads/Set1'

    # Gesture Recognition params
    GR_PARAMS_DIR = './data/.gr-params/'
    FILE_NAME_VOC = 'voc.pickle'
    FILE_NAME_HMM = 'hmm-s.pickle'
    FILE_NAME_GMM = 'gmm.pickle'

    # dhg marcel data set
    FILE_NAME_TRAIN = './data/dhg_marcel/train.dat'
    FILE_NAME_TEST = './data/dhg_marcel/test.dat'
    FILE_NAME_EVAL = './data/dhg_marcel/eval.dat'

    # Images formats
    IMAGES_ENDING = ['.jpg', '.png', '.ppm', '.pgm']

    @staticmethod
    def get_hsv_values():
        elem = SettingProvider.__TREE.find('.//HSVval')
        hsv_values = {
            attrib: int(elem.get(attrib))
            for attrib in SettingProvider.HSV_VALUES_NAMES
        }
        return hsv_values

    @staticmethod
    def set_hsv_values(hsv_values):
        elem = SettingProvider.__TREE.find('.//HSVval')
        for attrib in SettingProvider.HSV_VALUES_NAMES:
            elem.set(attrib, str(hsv_values[attrib]))

        SettingProvider.__TREE.write(SettingProvider.__SETTING_FILE_NAME, 'UTF-8')

    @staticmethod
    def load_hand_templates():
        hand_templates = {}
        templates = SettingProvider.__TREE.find('.//HandTemplates')
        directory = templates.get('directory')
        extension = templates.get('extension')
        names = gb.glob(directory + '*.' + extension)
        for name in names:
            res = cv2.imread(name, cv2.CV_LOAD_IMAGE_GRAYSCALE)
            base_name = path.basename(name).split('.')[0]
            hand_templates["left " + base_name] = res
            hand_templates["right " + base_name] = np.fliplr(res)
        return hand_templates

    @staticmethod
    def load_haar_cascades():
        names = gb.glob('../data/haar_cascades/*.xml')
        return {path.basename(name).split('.')[0]: cv2.CascadeClassifier(name)
                for name in names}

    @staticmethod
    def load_dhg_marcel_set(filename=FILE_NAME_TRAIN):
        def select_item(data, item_ind):
            for i in xrange(item_ind - len(data) + 1):
                data.append([])
            return data[item_ind]

        data = []
        with open(filename, 'r') as f:
            number_of_trajectories = int(f.readline())
            f.readline()
            f.readline()
            i = 0
            while i < number_of_trajectories:
                number_of_points = int(f.readline())
                l = []

                for point in xrange(number_of_points):
                    line = f.readline()
                    ind = int(line.split(' ')[2])
                    l.append(np.fromstring(line, dtype=np.float, sep=' ')[:2])

                select_item(data, ind).append(np.array(
                    cv2.normalize(np.array(l), norm_type=cv2.NORM_MINMAX)))
                i += 1

        return number_of_trajectories, data

    @staticmethod
    def load_models():
        elem = SettingProvider.__TREE.find('./HMMs')
        num_models = int(elem.get('num_models'))
        models = []
        gaussian_hmm_tags = SettingProvider.__TREE.findall('./HMMs/GaussianHMM')

        try:
            for id in xrange(num_models):
                file_name = gaussian_hmm_tags[id].get('params_file')
                with open(file_name, 'rb') as f:
                    model = pickle.load(f)
                models.append(model)
        except IOError:
            logging.info("training models... waiting...")
            from seqclassif import trainer
            s_trainer = trainer.SequenceTrainer(n_segments=32, max_jump=1)

            s_trainer.train(SettingProvider.load_dhg_marcel_set()[1])
            s_trainer.save()

            return SettingProvider.load_models()

        return models

    @staticmethod
    def write_models_params(models):
        SettingProvider.__TREE.find('./HMMs').set('num_models', str(len(models)))
        gaussian_hmm_tags = SettingProvider.__TREE.findall('./HMMs/GaussianHMM')
        models_dir = '../data/hmms/'
        if not path.exists(models_dir):
            makedirs(models_dir)
        for id in xrange(len(models)):
            gaussian_hmm_tags[id].set('id', str(id))
            gaussian_hmm_tags[id].set('n_segments', str(len(models[id].startprob_)))
            file_name = models_dir + 'hmm' + str(id) + '.pickle'
            gaussian_hmm_tags[id].set('params_file', file_name)
            with open(file_name, 'wb') as f:
                pickle.dump(models[id], f)
        SettingProvider.__TREE.write(SettingProvider.__SETTING_FILE_NAME, 'UTF-8')

    @staticmethod
    def load_all_files(directory, file_ending):
        files = []
        for file_name in listdir(directory):
            full_file_name = path.join(directory, file_name)
            if file_name.endswith(file_ending):
                img = cv2.imread(full_file_name, cv2.CV_LOAD_IMAGE_GRAYSCALE)
                files.append(img)
        return files

    @staticmethod
    def load_train_dict(root_images_dir=ROOT_TRAIN_DIR, subdirectories=None, img_ending='.ppm', nums=150):
        images_dict = defaultdict(list)

        dirs = subdirectories if subdirectories else listdir(root_images_dir)
        for dir_name in dirs:
            class_dir = path.join(root_images_dir, dir_name)

            if path.isdir(class_dir):
                images = SettingProvider.load_all_files(class_dir, img_ending)
                shuffle(images)

                images_dict[dir_name] = images[: min(len(images), nums)]

        return images_dict

    @staticmethod
    def load_test_dict(root_images_dir=ROOT_TEST_DIR, subdirectories=None,
                       subdirectory='uniform', img_ending='.ppm'):
        images_dict = defaultdict(list)
        dirs = subdirectories if subdirectories else listdir(root_images_dir)
        for dir_name in dirs:
            class_dir = path.join(root_images_dir, dir_name)
            if path.isdir(class_dir):
                images_dict[dir_name] = SettingProvider \
                    .load_all_files(path.join(class_dir, subdirectory), img_ending)
        return images_dict

    @staticmethod
    def save_bag_of_words_params(names, vocabulary, svm):
        if not path.exists(SettingProvider.BOW_DATA_DIR):
            makedirs(SettingProvider.BOW_DATA_DIR)
        for file_name, obj in zip((SettingProvider.FILE_NAME_POSES, SettingProvider.FILE_NAME_VOCABULARY),
                                  (names, vocabulary)):
            with open(file_name, 'wb') as f:
                pickle.dump(obj, f)
        svm.save(SettingProvider.FILE_NAME_SVM)

    @staticmethod
    def load_bag_of_words_params():
        try:
            with open(SettingProvider.FILE_NAME_POSES, 'rb') as f:
                names = pickle.load(f)
            with open(SettingProvider.FILE_NAME_VOCABULARY, 'rb') as f:
                vocabulary = pickle.load(f)
            svm_classifier = cv2.SVM()
            svm_classifier.load(SettingProvider.FILE_NAME_SVM)

        except IOError:
            logging.info("training bag of words - SVM classifier... waiting...")
            from bow.bowtr import BowTrainer

            trainer = BowTrainer(SettingProvider.VOCABULARY_SIZE)
            trainer.train(data=SettingProvider.load_train_dict())
            SettingProvider.save_bag_of_words_params(trainer.poses_names, trainer.descriptor_extractor.vocabulary,
                                                     trainer.svm)
            return SettingProvider.load_bag_of_words_params()
        return names, vocabulary, svm_classifier

    @staticmethod
    def save_gr_params(vocabulary, hmm_s, gmm, params_dir=GR_PARAMS_DIR):
        if not path.exists(params_dir):
            makedirs(params_dir)

        with open(path.join(params_dir, SettingProvider.FILE_NAME_VOC), 'wb') as f:
            pickle.dump(vocabulary, f)
        with open(path.join(params_dir, SettingProvider.FILE_NAME_HMM), 'wb') as f:
            pickle.dump(hmm_s, f)
        with open(path.join(params_dir, SettingProvider.FILE_NAME_GMM), 'wb') as f:
            pickle.dump(gmm, f)

    @staticmethod
    def load_gr_params(params_dir=GR_PARAMS_DIR):
        with open(path.join(params_dir, SettingProvider.FILE_NAME_VOC), 'rb') as f:
                vocabulary = pickle.load(f)
        with open(path.join(params_dir, SettingProvider.FILE_NAME_HMM), 'rb') as f:
                gmm = pickle.load(f)
        with open(path.join(params_dir, SettingProvider.FILE_NAME_GMM)) as f:
                hmm_s = pickle.load(f)

        return vocabulary, gmm, hmm_s

    @staticmethod
    def load_gesture_seqs(root_dir=GR_ROOT_DIR, file_end='.png'):
        """Loading gesture images from directory

        :param root_dir: path to parent directory with gestures
         path/
         path/gesture_name/
         path/gesture_name/0001/
         path/gesture_name/0001/frame0..n.jpg(.png)
        :return: list of gestures
        [[[image_0, image_1, ..., image_n],..],..]
        """
        seqs = []

        list_dir = listdir(root_dir)
        list_dir.sort()

        for dir in list_dir:
            sub_dir = path.join(root_dir, dir)
            logging.debug('sub_dir: %s', sub_dir)

            if path.isdir(sub_dir):
                seqs.append([])

                list_dir = listdir(sub_dir)
                list_dir.sort()

                for dir in list_dir:
                    sub_sub_dir = path.join(sub_dir, dir)

                    if path.isdir(sub_sub_dir):
                        logging.debug('sub_sub_dir: %s', sub_sub_dir)
                        seqs[-1].append(SettingProvider.load_all_files(sub_sub_dir, file_ending=file_end))
        return seqs
