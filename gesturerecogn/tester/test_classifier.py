from operator import add
import logging

import numpy as np


class Tester:
    @staticmethod
    def test(classify_fun, test_data):
        count = 0.0
        correct = 0.0

        nums = len(test_data)
        confusion_mat = np.array([[0] * nums] * nums, dtype=float)

        for i, data in enumerate(test_data):
            for ges in data:
                predict_num = classify_fun(ges)
                confusion_mat[i][predict_num] += 1.0

                correct += predict_num == i
                count += 1.0

        correct_results = round(correct / count * 100, ndigits=2)

        return correct_results, confusion_mat

    @staticmethod
    def cross_validation(trainer, data, p=5):
        gesture_num = len(data)

        results = []
        confusion_mats = []

        for x in xrange(p):
            nums = [len(d) for d in data]

            test_data = [data[ind][nums[ind] / p * x: nums[ind] / p * (x + 1)]
                         for ind in xrange(gesture_num)]
            train_data = [data[ind][: nums[ind] / p * x] + data[ind][nums[ind] / p * (x + 1):]
                          for ind in xrange(gesture_num)]

            trainer.train(train_data)

            classifier = trainer.classifier

            res, confusion_mat = Tester.test(classifier.classify, test_data)

            results.append(res)
            confusion_mats.append(confusion_mat)

            logging.info('confusion_matrix: \n%s', confusion_mat)
            logging.info('path %s\tP(w==class) = %s', x, res)

        result = np.mean(results)
        confusion_matrix = reduce(add, confusion_mats) / p

        for x in xrange(gesture_num):
            for y in xrange(gesture_num):
                confusion_matrix[x][y] = round(confusion_matrix[x][y])

        logging.info('cross valudation: %s\nconfusion_matrix:\n%s', result, confusion_matrix)

        return result, confusion_matrix
