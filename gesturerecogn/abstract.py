__author__ = 'valeria'


class AbstractClassifier(object):

    def classify(self, obj):
        raise NotImplementedError('Should have implemented method classify')


class AbstractTrainer(object):

    def train(self, train_data):
        raise NotImplementedError('Should have implemented method train')

    @property
    def classifier(self):
        raise NotImplementedError('Should have implemented property classifier')
