import logging

import cv2
from sklearn import mixture
from gesturerecogn import DescriptorExtractor
from gesturerecogn.abstract import AbstractTrainer

from gesturerecogn.bow.voctr import VocabularyTrainer


class BowGmmHmmTrainer(AbstractTrainer):

    def __init__(self, voc_size, vector_size, descriptor, n_segments=32, max_jump=1):
        """ Gesture Recognition Trainer

        :param n_segments: int
            Number of segments in HMM training process
        :param max_jump: int
            Max jump in HMM training process
        :param voc_size: int
            Number of cluster count in k-means features clustering
        :param vector_size: int
            Number of cluster count in GMM histogram clustering
        :param descriptor: OpenCV descriptor
            cv2.SIFT() or cv2.SURF() or other
        """
        self.__voc_size = voc_size
        self.__descriptor = descriptor
        self.__num_components = vector_size
        self.__n_segments = n_segments
        self.__max_jump = max_jump

        # saving params
        self._gmm = None
        self._hmm_s = None
        self._vocabulary = None

    def _bow_process(self, data):
        """Bag of words process:
            1. Compute features for each image in data
            2. K-means features clustering and create Vocabulary
            3. Create DescriptorExtractor and compute histogram for each image

        :param data: list of gestures: [[[frame_0, frame_1, ..], ..], ..]
        :return: list of histograms: [[[hist_0, hist_1, ..], ..], ..]
        """
        logging.info('Bag of words process')

        vocabulary_trainer = VocabularyTrainer(cluster_count=self.__voc_size)

        logging.info('Compute features for each image in data')
        features = [[[self.__descriptor.detectAndCompute(frame, mask=None)[1]
                      for frame in frames] for frames in gesture] for gesture in data]

        for gesture in features:
            for frames in gesture:
                for feature in frames:
                    vocabulary_trainer.add_descriptors(feature)

        logging.info('K-means features clustering')
        self._vocabulary = vocabulary_trainer.cluster()

        logging.info('Create DescriptorExtractor and compute histogram for each image')
        extractor = DescriptorExtractor(self._vocabulary)

        hists = [[[extractor.calculation_hist(feature, normalize=True)
                   for feature in frames] for frames in gesture] for gesture in features]

        return hists

    def _gmm_process(self, data):
        """GMM process:
            1. Create and train GMM
            2. Compute gmm.predict_proba for each histogram

        :param data: list of histograms: [[[hist_0, hist_1, ..], ..], ..]
        :return: list of gmm.predict_proba vectors: [[[gmm_v_0, gmm_v_1, ..], ..], ..]
        """
        logging.info('GMM process')
        logging.info('Create and train GMM')

        self._gmm = mixture.GMM(n_components=self.__num_components)

        em_data = []
        for gesture in data:
            for hists in gesture:
                for hist in hists:
                    em_data.append(hist)
        self._gmm.fit(em_data)

        logging.info('Compute gmm.predict_proba for each histogram')
        gmm_vectors = [[self._gmm.predict_proba(seq) for seq in seqs] for seqs in data]

        return gmm_vectors

    def _hmm_process(self, data):
        """ HMM process:
            1. Estimate HMM params: start_prob, trans_mat, means, covariance
            2. Create GaussianHMM models and train model on the data

        :param data: Train data
        :return:
        """
        from gesturerecogn.seqclassif import trainer
        seq_trainer = trainer.SequenceTrainer(
            n_segments=self.__n_segments,
            max_jump=self.__max_jump)

        logging.info('HMM process')
        self._hmm_s = seq_trainer.train(data)

    def train(self, train_data):
        """Train Gesture Recognizer

        :param train_data:
        :return:
        """
        logging.info('Train Gesture Recognizer...\tnumber of gestures: %s', len(train_data))

        hists = self._bow_process(train_data)
        gmm_vectors = self._gmm_process(hists)
        self._hmm_process(gmm_vectors)

    def save_params(self):
        from gesturerecogn import DescriptorExtractor, SettingProvider
        SettingProvider.save_gr_params(self._vocabulary, self._hmm_s, self._gmm)

    @property
    def vector_size(self):
        return self.__num_components

    @vector_size.setter
    def vector_size(self, value):
        self.__num_components = value

    @property
    def n_segments(self):
        return self.__n_segments

    @n_segments.setter
    def n_segments(self, value):
        self.__n_segments = value

    @property
    def descriptor(self):
        return self.__descriptor

    @property
    def classifier(self):
        from classifier import BowGmmHmmClassifier

        return BowGmmHmmClassifier(
            vocabulary=self._vocabulary,
            gmm=self._gmm,
            hmm_s=self._hmm_s,
            descriptor=self.descriptor)
