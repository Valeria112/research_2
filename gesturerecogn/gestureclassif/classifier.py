from gesturerecogn import DescriptorExtractor, SequenceClassifier
from gesturerecogn.abstract import AbstractClassifier


class BowGmmHmmClassifier(AbstractClassifier):
    def __init__(self, vocabulary, descriptor, gmm=None, hmm_s=None):
        self.__extractor = DescriptorExtractor(vocabulary)
        self.__gmm = gmm
        self.__hmm_recognizer = SequenceClassifier(hmm_s)
        self.__descriptor = descriptor

    def classify(self, frames):
        data = self.calc_hists_from_frames(frames)

        return self.classify_from_hists(hists=data)

    def classify_from_hists(self, hists):
        return self.__hmm_recognizer.predict(
            self.__gmm.predict_proba(hists),
            norm_traject=False)

    def calc_hists_from_frames(self, frames):
        data = []
        for frame in frames:
            kp, des = self.__descriptor.detectAndCompute(frame, mask=None)
            data.append(self.__extractor.calculation_hist(des, normalize=True))

        return data

    def classify_gmm_vectors(self, vectors):
        return self.__hmm_recognizer.predict(
            vectors, norm_traject=True)

    def get_gmm_vec(self, image):
        kp, des = self.__descriptor.detectAndCompute(image, mask=None)
        hist = self.__extractor.calculation_hist(des, normalize=True)

        return self.__gmm.predict_proba([hist])[0]

    @property
    def vocabulary(self):
        return self.__extractor.vocabulary

    @vocabulary.setter
    def vocabulary(self, voc):
        self.__extractor = DescriptorExtractor(voc)

    @property
    def gmm(self):
        return self.gmm

    @gmm.setter
    def gmm(self, gmm):
        self.__gmm = gmm

    @property
    def hmms(self):
        return self.__hmm_recognizer.models

    @hmms.setter
    def hmms(self, models):
        self.__hmm_recognizer = SequenceClassifier(models)

    @property
    def descriptor(self):
        return self.__descriptor

    @descriptor.setter
    def descriptor(self, desc):
        self.__descriptor = desc
