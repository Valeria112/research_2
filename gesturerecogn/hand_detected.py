import cv2
import copy as cp

from setting_provider import SettingProvider
from functions import nothing


class HandDetected():
    __HAND_TEMPLATES = SettingProvider.load_hand_templates()
    __MINIMAL_PROB = 75
    __MINIMAL_SKIN_PIXELS = 70
    __CELL_SIZE = (25, 25)
    __MINIMAL_WINDOW_SIZE = (2 * __CELL_SIZE[0], 2 * __CELL_SIZE[1])
    __MINIMAL_CONTOURS_LEN = 2 * (__MINIMAL_WINDOW_SIZE[0] + __MINIMAL_WINDOW_SIZE[1])
    __MINIMAL_CONTOURS_AREA = __MINIMAL_WINDOW_SIZE[0] * __MINIMAL_WINDOW_SIZE[1]
    __CASCADES = SettingProvider.load_haar_cascades().values()[1:2]

    # hand poses names
    TEMPLATES_NAMES = __HAND_TEMPLATES.keys()
    # hand detected methods
    HAND_TEMPLATE_CONTOURS = 0
    HAND_TEMPLATE_SCANNING_WINDOW = 1
    HAAR_CASCADE = 2

    @staticmethod
    def __find_roi(img, method, size=(0, 0)):
        list_roi = []
        if method == HandDetected.HAND_TEMPLATE_CONTOURS:
            bin_img_ = cp.deepcopy(img)
            contours, hierarchy = cv2.findContours(bin_img_, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            for c in contours:
                # if len(c) > HandDetected.__minimal_contours_len:
                if cv2.contourArea(c) > HandDetected.__MINIMAL_CONTOURS_AREA:
                    x, y, w, h = cv2.boundingRect(c)
                    list_roi.append([img[y:y + h, x:x + w], (x, y, w, h)])
        elif method == HandDetected.HAND_TEMPLATE_SCANNING_WINDOW:
            h_image, w_image = size
            for x_size in xrange(HandDetected.__MINIMAL_WINDOW_SIZE[0], w_image, HandDetected.__CELL_SIZE[0]):
                for y_size in xrange(HandDetected.__MINIMAL_WINDOW_SIZE[1], h_image, HandDetected.__CELL_SIZE[1]):
                    for x in xrange(0, w_image, HandDetected.__CELL_SIZE[0]):
                        for y in xrange(0, h_image, HandDetected.__CELL_SIZE[1]):
                            if x + x_size < w_image and y + y_size < h_image and 0.5 < float(x_size) / y_size < 2:
                                new_img = img[y:y + y_size, x:x + x_size]
                                if 0.4 < cv2.countNonZero(new_img) / float(new_img.size) < 0.7:
                                    list_roi.append([new_img, (x, y, x_size, y_size)])
        return list_roi

    @staticmethod
    def __comparison_templates(roi):
        img = roi[0]
        region = roi[1]
        most_likely_template = ''
        max_prob = 0

        compare = lambda template, roi_: \
            100 - cv2.countNonZero(cv2.bitwise_xor(template, roi_)) * 100 / template.size
        for temp_name in HandDetected.TEMPLATES_NAMES:
            temp = HandDetected.__HAND_TEMPLATES[temp_name]
            w, h = temp.shape[::-1]
            w_roi, h_roi = img.shape[::-1]

            if 0.8 < float(w * h_roi) / (h * w_roi) < 1.2:
                temp_prob = compare(temp, cv2.resize(img, (w, h)))
            elif h_roi > w_roi:
                roi[1] = (region[0], region[1], w_roi, h * w_roi / w)
                temp_prob = compare(temp, cv2.resize(img[0:h * w_roi / w, 0:w_roi], (w, h)))
            else:
                continue
            if temp_prob > max_prob:
                most_likely_template = temp_name
                max_prob = temp_prob

        if len(most_likely_template) and max_prob > HandDetected.__MINIMAL_PROB:
            return most_likely_template
        else:
            return None

    @staticmethod
    def detect(frame, mask, method=HAND_TEMPLATE_CONTOURS):
        templates_on_frame = []

        def hand_template():
            list_roi = HandDetected.__find_roi(mask, method, frame.shape[:-1])
            if not len(list_roi):
                return []
            for roi in list_roi:
                name = HandDetected.__comparison_templates(roi)
                if not name:
                    continue
                templates_on_frame.append(roi[1])
            return templates_on_frame

        def haar_cascade():
            import cv2.cv as cv

            gray = cv2.equalizeHist(cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY))
            for cascade in HandDetected.__CASCADES:
                for x, y, w, h in cascade.detectMultiScale(gray, scaleFactor=1.05, minNeighbors=4,
                                                           minSize=HandDetected.__MINIMAL_WINDOW_SIZE,
                                                           flags=cv.CV_HAAR_SCALE_IMAGE):
                    if cv2.countNonZero(mask[y:y + h, x:x + w]) * 100 / (w * h) > HandDetected.__MINIMAL_SKIN_PIXELS:
                        templates_on_frame.append((x, y, w, h))
            return templates_on_frame

        switch = [hand_template, hand_template, haar_cascade, nothing]
        index = lambda i: i if i in xrange(len(switch)) else -1

        return switch[index(method)]()