import math


nothing = lambda x: x

hypotenuse = lambda x, y: math.sqrt(x ** 2 + y ** 2)

low_point = lambda i, size: min(i, size)
upper_point = lambda i: max(i, 0)
