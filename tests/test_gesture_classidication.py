import logging
import cv2

from gesturerecogn import SettingProvider
from gesturerecogn.gestureclassif.trainer import BowGmmHmmTrainer
from gesturerecogn.tester.test_classifier import Tester

logging.basicConfig(filename='training.log',
                    level=logging.INFO)

data = SettingProvider.load_gesture_seqs(file_end='.jpg')

sift = cv2.SIFT(
    contrastThreshold=0.02,
    edgeThreshold=20,
    nOctaveLayers=5,
    sigma=2.5)

trainer = BowGmmHmmTrainer(
    voc_size=100,
    vector_size=9,
    n_segments=32,
    descriptor=sift)

correct_results, confusion_mat = Tester.cross_validation(trainer, data, p=3)

logging.info('correct results: %s\nconfusion matrix:\n%s',
             correct_results, confusion_mat)
