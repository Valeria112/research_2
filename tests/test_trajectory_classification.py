import logging
from random import shuffle

logging.basicConfig(level=logging.INFO)

from gesturerecogn.tester.test_classifier import Tester
from gesturerecogn import SettingProvider
from gesturerecogn.seqclassif.trainer import SequenceTrainer


n1, data1 = SettingProvider.load_dhg_marcel_set()
n2, data2 = SettingProvider.load_dhg_marcel_set(SettingProvider.FILE_NAME_TEST)
n3, data3 = SettingProvider.load_dhg_marcel_set(SettingProvider.FILE_NAME_EVAL)

gesture_nums = len(data1)
data = [data1[x] + data2[x] + data3[x] for x in xrange(gesture_nums)]

for gestures in data:
    shuffle(gestures)

Tester.cross_validation(
    SequenceTrainer(n_segments=32, max_jump=1),
    data)
