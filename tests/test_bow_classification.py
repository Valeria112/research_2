import logging

from gesturerecogn import SettingProvider
from gesturerecogn.bow.bowtr import BowTrainer
from gesturerecogn.tester.test_classifier import Tester

logging.basicConfig(level=logging.INFO)

images_dict = SettingProvider.load_train_dict(
    root_images_dir=SettingProvider.MY_HAND_SET_DIR,
    img_ending='.png', nums=250)

Tester.cross_validation(BowTrainer(vocabulary_size=100),
                        data=images_dict.values(), p=5)
