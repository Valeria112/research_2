import logging
logging.basicConfig(level=logging.INFO)

from gesturerecogn import *


poses_names, vocabulary, svm_classifier = SettingProvider.load_bag_of_words_params()
bow_classifier = BowClassifier(svm_classifier, DescriptorExtractor(vocabulary))

# recognizer trajectories
hmm_classifier = SequenceClassifier(SettingProvider.load_models())

# minimal track length in pixels
MINIMAL_TRACK_LEN = 250
# maximum track length in pixels
MAXIMUM_TRACK_LEN = 750

cv2.namedWindow('capture')
frame = None

cap = cv2.VideoCapture(0)
if not cap.isOpened():
    logging.warning("Couldn't open camera")
    exit(0)

hsv_val = SettingProvider.get_hsv_values()
for name in SettingProvider.HSV_VALUES_NAMES:
    cv2.createTrackbar(name, 'capture', 0, 255, nothing)
    cv2.setTrackbarPos(name, 'capture', hsv_val[name])

bin_img = False
gauss = False
blur = False
median = False

frequency_detect = 10

print('''
Press 'w' to switch in binary image
Press 'g' to apply Gaussian Filtering
Press 'b' to apply Image Blurring
Press 'm' to apply Median Filtering
Press 's' to save current hsv values
''')

current_hands = CurrentHands()
tracker = Tracker(current_hands)
mouse_sync = MouseSync()

morphology = MorphTransform((5, 5))
morphology_iter = 8

main_meter = Meter()

while True:
    success, frame = cap.read()
    if not success:
        logging.warning('Frame not read')
        break

    frame = cv2.flip(frame, 1)

    if main_meter.frame_count == 0:
        mouse_sync.set_frame_size((frame.shape[1], frame.shape[0]))

    main_meter.update()
    logging.debug('fps: %d last_frame_time: %s, mean_frame_time: %s',
                  main_meter.fps, main_meter.last_frame_time, main_meter.mean_frame_time)

    # Skin Segmentation
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    hsv_new_val = {
        name: cv2.getTrackbarPos(name, 'capture')
        for name in SettingProvider.HSV_VALUES_NAMES}
    values = tuple(
        hsv_new_val[name]
        for name in SettingProvider.HSV_VALUES_NAMES)

    def color_tuple(i):
        if i % 2:
            tup = [255] * 3
        else:
            tup = [0] * 3
        tup[i / 2] = values[i]
        return tuple(tup)

    masks = [cv2.resize(cv2.inRange(hsv, color_tuple(i), color_tuple(i + 1)), (0, 0), fx=0.5, fy=0.5)
             for i in range(0, 5, 2)]

    mask = cv2.inRange(hsv, values[::2], values[1::2])
    mask = morphology.apply_all(mask, morphology_iter)

    # Smoothing Image
    kernel_size = (11, 11)
    if gauss:
        frame = cv2.GaussianBlur(frame, kernel_size, 0)
    if median:
        frame = cv2.medianBlur(frame, kernel_size[0])
    if blur:
        frame = cv2.blur(frame, kernel_size)

    # hand look at every $frequency_detect frame
    if main_meter.frame_count % frequency_detect == 0:
        detects = HandDetected.detect(frame, mask)
        # detects = HandDetected.detect(frame, mask, method=HandDetected.HAAR_CASCADE)

        if len(detects):
            for x, y, w, h in detects:
                current_hands.add_hand(frame[y:y + h, x:x + w], mask[y:y + h, x:x + w],
                                       Point(x + w / 2, y + h / 2), meter=main_meter)

    tracker.update(frame, method=Tracker.MEAN_SHIFT)

    for hand in current_hands:
        x, y, dx, dy = hand.track_window

        # bag of words classification
        image = cv2.cvtColor(frame[y: y + dy, x: x + dx], cv2.COLOR_RGB2GRAY)
        image_mask = mask[y: y + dy, x: x + dx]
        key_ind = int(bow_classifier.classify(image, mask=image_mask))
        pose_name = poses_names[key_ind]

        # mouse move
        # mouse_x, mouse_y = mouse_sync.translate_coordinates(hand.last_location)
        # mouse_sync.move(mouse_x, mouse_y)

        cv2.rectangle(frame, (x, y), (x + dx, y + dy), ALL_COLORS.RED, 3)
        cv2.rectangle(mask, (x, y), (x + dx, y + dy), ALL_COLORS.WHITE[0], 3)
        Drawer.draw_track(hand.locations, frame, ALL_COLORS.GREEN, visible_points=20)

        cv2.putText(frame, '{0} v={1}'.format(pose_name, round(hand.velocity)),
                    (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, ALL_COLORS.WHITE,
                    thickness=2, bottomLeftOrigin=False)

        if hand.len_trajectory > MINIMAL_TRACK_LEN:
            gesture_class, is_gesture = hmm_classifier.predict_diff(hand.get_trajectory, True)

            if is_gesture:
                logging.info('gesture_class: %s', gesture_class)

                gesture_image = Drawer.draw_trajectory(
                    trajectory=cv2.normalize(hand.get_trajectory, norm_type=cv2.NORM_MINMAX),
                    color=GESTURE_COLORS.get_gesture_color(gesture_class),
                    thickness=2)

                # cv2.imshow('gesture recognizer', gesture_image)

                hand.reset_locations()

        elif hand.len_trajectory > MAXIMUM_TRACK_LEN:
            hand.reset_locations()

    masks.append(cv2.resize(mask, (0, 0), fx=0.5, fy=0.5))

    if bin_img:
        h, w = mask.shape[::-1]
        mask[: w / 2, : h / 2] = masks[0]
        mask[: w / 2, h / 2: h] = masks[1]
        mask[w / 2: w, : h / 2] = masks[2]
        mask[w / 2: w, h / 2: h] = masks[3]
        mask = cv2.inRange(mask, ALL_COLORS.GRAY[0], ALL_COLORS.WHITE[0])

        cv2.imshow('capture', morphology.apply_all(mask, morphology_iter))
    else:
        cv2.imshow('capture', frame)

    key = cv2.waitKey(1) & 0xFF
    if key == 27:
        break
    elif key == ord('s'):
        logging.info('save hvs values')
        SettingProvider.set_hsv_values(hsv_new_val)
    elif key == ord('w'):
        logging.info('switch')
        bin_img = not bin_img
    elif key == ord('g'):
        gauss = not gauss
        logging.info('Gaussian Filtering:%s', gauss)
    elif key == ord('m'):
        median = not median
        logging.info('Median Filtering:%s', median)
    elif key == ord('b'):
        blur = not blur
        logging.info('Image Blurring:%s', blur)


cap.release()
cv2.destroyAllWindows()
